<div class="content">
    <div class="main_content">
        <div class="user_form user_forgot">
            <form method="POST" action="/ajax/users/send_sms_forgot">
                <input type="hidden" name="act" value="login"/>
                <div class="title">Восстановление пароля</div>
                <div class="content_loader_hide">
                    <div class="div_input"><input type="text" name="email" placeholder="E-mail"/></div>
                    <div class="error" rel="send_restore" style="margin-top: 10px;"></div>
                    <div class="btn block send_restore">Далее</div>
                    <div class="step_forgot_2" style="display:none;">
                        <div class="div_text">На ваш E-mail оправлено письмо с кодом</div>
                        <div class="div_input"><input type="text" name="code" placeholder="Введите код из письма"/></div>
                        <div class="div_input"><input type="password" name="pass" placeholder="Новый пароль"/></div>
                        <div class="div_input"><input type="password" name="pass1" placeholder="Повторите новый пароль"/></div>
                        <div class="error" rel="confirm_restore" style="display: block;margin-top: 20px;"></div>
                        <div class="btn block confirm_restore">Установить пароль</div>
                    </div>
                    <div class="term_privacy_text">
                        Нажимая на кнопку, вы даете
                        <a target="_blank" href="/confidential">согласие на обработку своих персональных данных</a>
                    </div>
                    <a href="/lk" style="margin-top: 30px;" class="register">Вход</a>
                </div>
                <div class="loader_login_content"><i class="fa fa-spinner fa-spin fa-4x"></i></div>
            </form>
        </div>
    </div>
</div>