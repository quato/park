<div class="content">
    <div class="main_content">
        <div class="user_form user_reg">
            <form method="POST" action="/ajax/users/send_reg">
                <input type="hidden" name="act" value="login"/>
                <div class="title">Регистрация</div>
                <div class="content_loader_hide">
                    <input type="hidden" name="hash" value=""/>
                    <div class="div_input"><input type="text" name="name" placeholder="Имя компании"/></div>

                    <div class="div_input" style="margin-top: 15px;"><input type="password" name="pass" placeholder="Пароль"/></div>
                    <div class="div_input"><input type="password" name="pass1" placeholder="Подтвердите пароль"/></div>

                    <div class="div_input" style="margin-top: 15px;"><input type="text" name="email" placeholder="Логин"/></div>

                    <div class="error" rel="send" style="margin-top: 10px;"></div>
                    <div class="btn block send_reg">Далее</div>
                    <div class="step_reg_2" style="display:none;">
                        <div class="div_text">На ваш E-mail оправлено письмо с кодом</div>
                        <div class="div_input"><input type="text" name="code" placeholder="Введите код из письма"/></div>
                        <div class="error" rel="reg" style="margin-top: 10px;"></div>
                        <div class="btn block confirm_reg">Зарегистрироваться</div>
                    </div>
                    <div class="term_privacy_text">
                        Нажимая на кнопку, вы даете<br/>
                        <a target="_blank" href="/confidential">согласие на обработку своих персональных данных</a>
                    </div>
                    <a href="/lk" class="link_login">Вход</a>
                </div>
                <div class="loader_login_content"><i class="fa fa-spinner fa-spin fa-4x"></i></div>
            </form>
            <?php echo md5('12345678gfg5'); ?>
        </div>
    </div>
</div>