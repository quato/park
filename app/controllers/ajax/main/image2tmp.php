<?php
global $userid;
if (!$user->auth) {
    $arr['status']='error';
    $arr['message']='Вы не авторизованы';
}
if ($arr['status']=='ok') {
    $rez = \s\Forms::LoadImage2Tmp($_FILES['img']);
    if ($rez['status']=='error') {
        $arr['status']='error';
        $arr['message']=$rez['message'];
    }
}
if ($arr['status']=='ok') {
    list($width, $height) = getimagesize($_SERVER['DOCUMENT_ROOT'].'/tmp/'.$rez['filename']);
    $arr = array_merge($arr, [
        'filename' => $rez['filename'],
        'url' => $rez['url'],
        'width' => $width,
        'height' => $height
    ]);
}