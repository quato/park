<?php
namespace s;
/**
 * Работа с посетителями
 * Class Visitors
 */
class Visitors extends \m\Visitors {
    function GetRightCompany($company_id, $edit = true) {
        global $user;
        $arr = ['status'=>'ok'];
        if ($user->data['active']==0) {
            $arr['status'] = 'error';
            $arr['message'] = 'Аккаунт компании приостановлен. Обратитесь к администратору';
        }
        if ($arr['status']=='ok') {
            if (!(($user->data['type'] == 3) ||
                ((!$edit)&&($user->data['type'] == 2)) ||
                (($user->data['type'] == 1) && ($company_id == $user->id)) ||
                (($user->data['type'] == 0) && (in_array($company_id, $user->companies_ids)))
            )) {
                $arr['status'] = 'error';
                $arr['message'] = 'Нет доступа. Обратитесь к администратору';
            }
        }
        return $arr;
    }
    function GetRight($edit = true) {
        return $this->GetRightCompany($this->data['company_user_id'], $edit);
    }
    public static function FormatDate($date) {
        if (date('Y-m-d', strtotime($date))==date('Y-m-d'))
            $status = 'в '.date('H:i', strtotime($date));
        else
            $status = date('d.m.Y', strtotime($date)).' '.date('H:i', strtotime($date));
        return $status;
    }
}
