<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} else {
    if ($user->type!=3) {
        if ($user->type==0)
        {
            $_POST['company_user_id'] = $user->companies_ids[0];
        }

        else
            $_POST['company_user_id'] = $user->id;
        if (!$_POST['company_user_id']) {
            $arr['status'] = 'error';
            $arr['message'] = 'Выберите компанию';
        }
    }
    if ($_POST['date']) {
        if (strtotime($_POST['date'])<strtotime(date('Y-m-d'))) {
            $arr['status'] = 'error';
            $arr['message'] = 'Нельзя выбрать дату которая уже прошла';
        }
        if (strtotime($_POST['date']) > time() + 3600*24*31){
            $arr['status'] = 'error';
            $arr['message'] = 'Нельзя выбрать дату больше чем на месяц вперёд';
        }
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите дату';
    }
}
if ($arr['status'] == 'ok') {
    if ((isset($_FILES['list']['tmp_name']))&&($_FILES['list']['tmp_name'] != '')) {
        $arr = \s\Forms::CheckFile($_FILES['list']['tmp_name'], ['txt', 'doc']);
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Файл не загружен. Возможно его размер больше ' . ini_get('upload_max_filesize');
    }
}

    


if ($arr['status'] == 'ok') {
    $dates = [];
    if ($_POST['type_date'] == 1) {
        $date = strtotime($_POST['date_from']);
        $date_to = strtotime($_POST['date_to']);
        if ($date > $date_to) {
            $arr['status'] = 'error';
            $arr['message'] = 'Дата начала периода не может быть меньше даты конца';
        } else {
            
            while ($date <= $date_to) {
                $dates[] = date('Y-m-d', $date);
                $date += 3600 * 24;
            }
        }
    } else {
        $dates = [date('Y-m-d', strtotime($_POST['date']))];
    }
}
if ($arr['status'] == 'ok') {
    $u = new \s\Visitors();
    if ($arr['type']=='txt') {
        //$fd = file_get_contents($_FILES['list']['tmp_name']);
       
        $fd = fopen($_FILES['list']['tmp_name'], 'rb');
        //$get_encod = mb_detect_encoding($fd);
        if ($fd) {
            while (!feof($fd)) {
                $str = fgets($fd);
                //$str = mb_convert_encoding($str,  "cp1251", 'UTF-8');
                $str = mb_convert_encoding($str, 'UTF-8', "cp1251");
                $data = explode(' ', $str);
                foreach ($dates as $date) {
                    $params = [
                        'name' => $data[0],
                        'fename' => $data[1],
                        'company_user_id' => intval($_POST['company_user_id']),
                        'date' => $date,
                        'date_add' => date('Y-m-d H:i:s')
                    ];
                    $u->GetOne($params);
                    if (!$u->id) {
                        $u->Insert($params);
                    }
                }
            }
            fclose($fd);
            // $fd = file_get_contents($_FILES['list']['tmp_name']);
            // if ($fd) {
            //     $get_encod = mb_detect_encoding($fd);
            //     $fd = iconv($get_encod,'UTF-8',$fd);
            //     $filesop =  explode(PHP_EOL, $fd);
            //     if (count($filesop) >0){
            //         foreach ($filesop as $stringa){
            //             $str = $stringa;
            //             $str = mb_convert_encoding($str,  "cp1251", 'UTF-8');
            //             $str = mb_convert_encoding($str, 'UTF-8', "cp1251");
            //             $data = explode(' ', $str);
            //             foreach ($dates as $date) {
            //                 $params = [
            //                     'name' => $data[0],
            //                     'fename' => $data[1],
            //                     'company_user_id' => intval($_POST['company_user_id']),
            //                     'date' => $date,
            //                     'date_add' => date('Y-m-d H:i:s')
            //                 ];
            //                 $u->GetOne($params);
            //                 if (!$u->id) {
            //                     $u->Insert($params);
            //                 }
            //             }
            //         }
            //     }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Ошибка открытие файла';
        }
    } else {
        require_once AppDir.'/includes/PhpSpreadsheet/src/Bootstrap.php';

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['list']['tmp_name']);
        $sheet = $spreadsheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $name = $sheet->getCell('A'.$row)->getValue();
            $fename = $sheet->getCell('B'.$row)->getValue();
            foreach ($dates as $date) {
                $params = [
                    'name' => $name,
                    'fename' => $fename,
                    'company_user_id' => intval($_POST['company_user_id']),
                    'date' => $date,
                    'date_add' => date('Y-m-d H:i:s')
                ];
                $u->GetOne($params);
                if (!$u->id) {
                    $u->Insert($params);
                }
            }
        }
    }
}