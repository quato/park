<?php
$arr['start'] = $_REQUEST['i'];
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
} elseif (!in_array($user->type, [2,3])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Нет доступа';
}
if ($arr['status'] == 'ok') {
    $countItems = 30;
    ob_start();
    $view['visitors'] = new \s\Visitors();
    $params = ['del' => 0];
    $weight = array(0=>'До 3.5т', 1=>'От 3.5т до 10т', 2=>'Больше 10т');
    $options = [
        'field_sql' => "",
        'table_sql' => "",
        'limit_count' => $countItems,
        'limit_from' => $_POST['i']
    ];
    if ($_POST['find']['text']) {
        $str = cstr($_POST['find']['text']);
        $w = '';
        $a = explode(" ", $str);

        if (count($a) == 2) {
            $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%' AND `visitors`.`fename` LIKE '" . $a[1] . "%') OR (`visitors`.`name` LIKE '" . $a[1] . "%' AND `visitors`.`fename` LIKE '" . $a[0] . "%')";
        } elseif (count($a) == 1) {
            $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%') OR (`visitors`.`fename` LIKE '" . $a[0] . "%') OR (`visitors`.`car_number` LIKE '" . $a[0] . "%')";
        } else {
            foreach ($a as $str) {
                $int = intval($str);
                $w .= ($int ? " OR `visitors`.`visitor_id`=$int" : "") . " OR `visitors`.`name` LIKE '$str%' OR `visitors`.`fename` LIKE '$str%'";
            }
        }
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($_POST['find']['company_user_id']) {
        $params['company_user_id'] = intval($_POST['find']['company_user_id']);
    }
    if ($_POST['find']['status']) {
        if ($_POST['find']['status']=='none') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NULL";
        } elseif ($_POST['find']['status']=='in') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL";
        } elseif ($_POST['find']['status']=='out') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NOT NULL";
        }
    } else {
        $options['where_sql'] .= " AND `visitors`.`date_out` is NULL";
    }

    if ($_POST['find']['type'] == 'car') {
        $options['where_sql'] .= " AND `visitors`.`car_number`!=''";
        $options['field_sql'] .= "`car_brand`.*,";
        $options['table_sql'] .= "LEFT JOIN `car_brand` ON `visitors`.`car_brand_id`=`car_brand`.`car_brand_id` ";
    }else {
        $options['where_sql'] .= " AND `visitors`.`car_number`=''";
    }
    $options['where_sql'] .= " AND (`visitors`.`date`='".date('Y-m-d')."' 
        OR (`visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL))";
    $options['field_sql'] .= '`visitors`.*, `users`.`name` as `company_name`, `users`.`image0` as `company_image`';
    $options['table_sql'] .= "LEFT JOIN `users` ON `visitors`.`company_user_id`=`users`.`user_id`";
    $options['where_sql'] .= " AND `users`.`del` = 0 AND `users`.`active` = 1";
    $rez = $view['visitors']->GetItems($params, $options);
    $arr['i'] = $_POST['i'] + $countItems;
    $arr['count'] = $view['visitors']->count;
    //$arr['ggg'] = count($a);
    foreach ($view['visitors']->items as $item) {
        // $item['name'] = mb_convert_encoding($item['name'],  "cp1251", 'UTF-8');
        // $item['name'] = mb_convert_encoding($item['name'], 'UTF-8', "cp1251");
        // $item['fename'] = mb_convert_encoding($item['fename'],  "cp1251", 'UTF-8');
        // $item['fename'] = mb_convert_encoding($item['fename'], 'UTF-8', "cp1251");
        $status = '';
        if ($item['date_out']) {
            $status = 'Вышел<br/>'.\s\Visitors::FormatDate($item['date_out']);
        } elseif ($item['date_in']) {
            $status = 'Вошел<br/>'.\s\Visitors::FormatDate($item['date_in']);
        }
        ?>
        <div
                class="item noclick"
                data-id="<?= $item['visitor_id'] ?>"
                rel="<?= ($item['date_out'] ? 'out' : ($item['date_in'] ? 'in' : 'none')) ?>">
            <div class="item_inner">
                <div class="title"><?= $item['fename'] ?> <?= $item['name'] ?> </div>
                <?php
                if ($item['car_number']!='') { ?>
                    <div style="font-size: 17px; margin-top: 10px"><b>Номер:</b> <input class="car_number" style="height: 10px" name="car_number" type="text" value="<?=$item['car_number']?>">
                        <p><b>Марка:</b> <?=$item['car_brand']?></p>
                        <p><b>Пассажиры:</b> <?=$item['passengers']?></p>
                        <p><label><b>Максимальный вес авто:</b> </label>
                            <select class="weight_id" name="weight_id">
                                <option <?=$item['weight']==0 ? 'selected="selected"' : ""?> value="0" >До 3.5т</option>
                                <option <?=$item['weight']==1 ? 'selected="selected"' : ""?> value="1" >От 3.5т до 10т</option>
                                <option <?=$item['weight']==2 ? 'selected="selected"' : ""?> value="2" >Больше 10т</option>
                            </select><p>
                    </div>
                <?php } ?>
            </div>
            <div class="right_item">
                <div class="company">
                    <div class="img" style="<?=($item['company_image'] ? $user->image_config[2]['url'].$item['company_image'] : '')?>"></div>
                    <div class="info">
                        <div class="ititle">Компания</div>
                        <div class="iname"><?=$item['company_name']?></div>
                    </div>
                </div>
                <div class="commands">
                    <div class="status" style="<?=($item['date_in'] ? '' : 'display:none;')?>">
                        <?= $status ?>
                    </div>
                    <div class="btn" style="<?=($item['date_out'] ? 'display:none;' : '')?>"><?= ($item['date_in'] ? 'Выпустить' : 'Впустить') ?></div>
                </div>
            </div>
        </div>
        <?php
    }
    $arr['html'] = ob_get_contents();
    ob_clean();
}