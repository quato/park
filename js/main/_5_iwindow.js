(function($,doc,win) {
    // If there's no jQuery, iwindow can't work
    if ( !$ ) {
        return;
    }
    // Check if iwindow is already initialized
    if ( $.fn.iwindow ) {
        $.error('iwindow already initialized');
        return;
    }
    $.fn.iwindow = function (type='load', data={}, callback=()=>{}) {
        let hide = (wnd) => {
            if (wnd.data('url')) {
                wnd.find('.window_content').html('');
            }
            wnd.removeClass('_showed').stop().fadeTo(500, 0, function(){
                $(this).hide();
            });
            $('body').css({'overflow': 'auto'});
        };
        let show = (wnd) => {
            wnd.addClass('_showed').stop();
            wnd.css('display', 'flex');
            wnd.css('opacity', 0);
            wnd.fadeTo(500, 1);
        };
        switch (type)
        {
            case 'load' :
                this.each(function(){
                    if ($(this).html()==='') {
                        $(this).html(`<div class="inner_content">
                            <div class="close"></div>
                            <div class="window_content"></div>
                        </div>`);
                    } else {
                        if (!$(this).find('.close').length)
                            $(this).find('.inner_content').prepend('<div class="close"></div>');
                    }
                });
                this.on('click', '.close, .close_btn', (e)=>{
                    hide($(e.currentTarget).closest('.window'));
                });
                this.on('mousedown', (e) => {
                    if ((!$(e.currentTarget).hasClass('static'))&&($(e.currentTarget).width() > e.clientX)) {
                        if (e.target == e.currentTarget) {
                            hide($(e.currentTarget));
                        }
                    }
                });
                break;
            case 'ok':
                this.find('.inner_content').hide();
                this.find('.inner_ok').show();
                break;

            case 'show' :
                this.find('.inner_content').show();

                if (this.data('url')) {
                    $.post(this.data('url'), data, (r) => {
                        let a = jQuery.parseJSON(r);
                        this.find('.window_content').html(a['html']);
                        callback(r);
                        show(this);
                    });
                } else {
                    show(this);
                }
                $('body').css({'overflow': 'hidden'});
                break;
            case 'hide' :
                hide(this);
                break;
        }
    }
}(jQuery, document, window));