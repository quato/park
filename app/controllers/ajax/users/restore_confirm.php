<?php
$arr['code']=$_POST['code'];

$users = new \s\Users();
$users->GetOne([
    'email'=>\s\Functions::FixEmail($_POST['email']),
    'del'=>0
]);
if (!$users->id) {
    $arr['status'] = 'error';
    $arr['message'] = 'Пользователя с таким E-mail не найдено';
}
if (($arr['status']=='ok')&&(!$_POST['code'])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Введите код из письма';
}
if (($arr['status']=='ok')&&($users->data['forgot_code']!=$_POST['code'])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Неправильный код из письма';
}
if (($arr['status']=='ok')&&(!$_POST['pass'])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Введите новый пароль';
}
if (($arr['status']=='ok')&&(!$_POST['pass1'])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Повторите новый пароль';
}
if (($arr['status']=='ok')&&($_POST['pass']!=$_POST['pass1'])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Пароли не совпадают';
}
if ($arr['status']=='ok') {
    $arr['user_id']=$users->data['user_id'];
    $users->Update(['pass' => \s\Users::GetPass($_POST['pass'])]);
    $users->AddAuth();
}