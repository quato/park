<?php

namespace m;


class CarVisitors extends \m\Models
{
    public $table_name='car_visitors';
    public $id_name='car_visitors_id';
    public $options = [
        'order'=>'`car_visitors`.`car_visitors_id` DESC'
    ];
}