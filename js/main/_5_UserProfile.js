class UserProfile {
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.btn_save = this.el.find('.save_profile');
            this.wndChangePass = $('.window[rel="change_pass"]');

            let Cropper_Image = new CropperImage(this.el.find('.image4crop'), (img_crop, img_full)=>{
                $.post('/ajax/users/load_image', {
                    img_crop: img_crop,
                    img_full: img_full
                }, (r)=>{
                    let a = jQuery.parseJSON(r);
                    if (a.status==='ok') {

                    } else {
                        alert(a.message);
                    }
                });
            });

            this.el.find('.change_pass').click(() => {
                this.wndChangePass.iwindow('show');
                this.wndChangePass.find('.change_pass_form').show();
                this.wndChangePass.find('.change_pass_text, .error').hide();
                this.wndChangePass.find('.change_pass_val').val('').focus();
            });
            this.wndChangePass.find('.change_pass_ok').click(() => {
                if (!this.wndChangePass.find('.change_pass_ok').hasClass('load')) {
                    this.wndChangePass.find('.change_pass_ok').formbutton('start');
                    this.wndChangePass.find('.error[rel="ok"]').html('');
                    $.post('/ajax/users/save_pass', {
                        pass: this.wndChangePass.find('.change_pass_val').val()
                    }, (r) => {
                        this.wndChangePass.find('.change_pass_ok').formbutton('stop');
                        let a = jQuery.parseJSON(r);
                        if (a.status === 'ok') {
                            this.wndChangePass.find('.change_pass_text').show();
                            this.wndChangePass.find('.change_pass_form').hide();
                        } else {
                            this.wndChangePass.find('.change_pass_form').show();
                            this.wndChangePass.find('.error[rel="ok"]').html(a['message']);
                        }
                    });
                }
            });
            this.btn_save.click(() => {
                if (!this.btn_save.hasClass('load')) {
                    this.btn_save.formbutton('start');

                    this.el.find('.error').html('');
                    this.el.find('.result_form').hide();

                    $.post('/ajax/users/save_profile', {
                        name: this.el.find('input[name="name"]').val(),
                        fename: this.el.find('input[name="fename"]').val()
                    }, (r) => {
                        this.btn_save.formbutton('stop');
                        let a = jQuery.parseJSON(r);
                        if (a.status === 'ok') {
                            this.el.find('.result_form').show().html('Профиль сохранен');
                            setTimeout(() => {
                                this.el.find('.result_form').hide();
                            }, 5000);
                        } else {
                            this.el.find('.error').html(a['message']);
                        }
                    });
                }
            });
            this.el.find('input').keyup((e) => {
                if(e.keyCode===13){
                    this.el.find('.save_profile').click();
                }
            });
        }
    }
}