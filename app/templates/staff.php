<?php if ($view['user']) { ?>
    <h1 class="page_title"><a href="?">Сотрудники</a> > <?=$view['title']?></h1>
    <div class="StaffAdmin_Edit">
        <input type="hidden" name="user_id" value="<?=$view['user']->id?>"/>
        <div class="card_form">
            <div class="form_item">
                <label class="form_control">Фотография</label>
                <div class="image4crop" data-name="image" data-image="<?=($view['user']->data['image0'] ? $view['user']->image_config[0]['url'].$view['user']->data['image0'] : '')?>"></div>
            </div>
            <div class="form_item">
                <label class="form_control">Имя</label>
                <input class="form_control" value="<?=$view['user']->data['name']?>" type="text" name="name"/>
            </div>
            <div class="form_item">
                <label class="form_control">Телефон</label>
                <input class="form_control" value="<?=$view['user']->data['phone']?>" type="text" name="phone"/>
            </div>
            <div class="form_item">
                <label class="form_control">
                    <input class="form_control" <?=($view['user']->data['active'] ? 'checked="checked"' : '')?> type="checkbox" name="active" value="1"/>
                    Открыт доступ
                </label>
            </div>
            <div class="form_item">
                <label class="form_control">Логин</label>
                <input class="form_control" value="<?=$view['user']->data['email']?>" type="text" name="email"/>
            </div>
            <div class="form_item">
                <label class="form_control"><?=(($view['user']->id) ? 'Новый пароль' : 'Пароль')?></label>
                <input class="form_control" value="" type="password" name="pass"/>
            </div>
        </div>
        <div class="error"></div>
        <?php if ($view['user']->id) { ?>
            <div class="form_button _cancel_form red delete">Удалить</div>
        <?php } ?>
        <duv class="form_button _save_form save">Сохранить</duv>
        <div style="clear:both;"></div>
    </div>
<?php } else { ?>
    <h1 class="page_title">Сотрудники</h1>
    <div class="LineItems StaffAdmin">
        <div class="items_finder">
            <div class="item_finder text_search">
                <input value="<?=cstr($_GET['text'])?>" class="text" placeholder="Поиск сотрудников" type="text"/>
                <div class="find_btn"><i class="fa fa-search" aria-hidden="true"></i></div>
            </div>
        </div>
        <a href="?id=add" class="form_button" style="margin-top: 20px;">Добавить сотрудника</a>
        <div class="items"></div>
        <div class="iload"></div>
    </div>
    <div class="window mini" rel="DeleteStaff">
        <div class="inner_content">
            <div class="close"><div class="iclose"></div></div>
            <div class="icontent">
                <h2>Удалить сотрудника?</h2>
                <div class="btn _cancel_form _grey close_btn">Отмена</div>
                <div class="btn _save_form red">Удалить</div>
            </div>
        </div>
    </div>
<?php } ?>