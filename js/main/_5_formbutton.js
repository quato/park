(function($,doc,win) {
    // If there's no jQuery, FormButton can't work
    if ( !$ ) {
        return;
    }
    // Check if FormButton is already initialized
    if ( $.fn.formbutton ) {
        $.error('formbutton already initialized');
        return;
    }
    $.fn.formbutton = function (type) {
        if (typeof (type) == 'undefined') type = 'load';
        switch (type)
        {
            case 'load' :
                this.each(function () {
                    var title = $(this).html();
                    $(this).html('<div class="t">' + title + '</div><div class="l"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></div>');
                    if ($(this).hasClass('submit')) {
                        $(this).on('click', function () {
                            $(this).formbutton('start');
                        });
                    }
                });
                break;
            case 'start' :
                var t = this.find('.t');
                var l = this.find('.l');
                this.addClass('load');
                l.css('width', t.outerWidth());
                break;
            case 'stop' :
                this.removeClass('load');
                this.find('.t').css('width', 'auto');
                break;
        }
    }
}(jQuery, document, window));