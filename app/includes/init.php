<?php
include AppDir.'/../vendor/autoload.php';
$dbc = new \s\db();
$db = $dbc->connect();
function cstr($s) {
    global $db;
    return mysqli_real_escape_string($db, trim(strip_tags(strval($s))));
}
function cs($s) {
    global $db;
    return trim(strip_tags(strval($s)));
}
function json_encode_x ($in) {
    return json_encode($in, JSON_UNESCAPED_UNICODE);
}
$user = new \s\Users();
$user->CheckAuth($_COOKIE['user_id'], $_COOKIE['user_session_id'], $_COOKIE['user_session_pass']);
