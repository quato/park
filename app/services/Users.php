<?php
namespace s;

use m\UserSessions;

class Users extends \m\Users {
    public $session = null;
    public $auth = false;
    public $type = 0;
    public $companies;
    public $companies_ids = [];
    public $types = [
        0=>'Сотрудник',
        1=>'Компания',
        2=>'Вахтер',
        3=>'Администратор'
    ];

    public $image_config = [
        ['url' => '/upload/users/full/', 'w' => 1920, 'h' => 1080],
        ['url' => '/upload/users/300/', 'w' => 300, 'h' => 300],
        ['url' => '/upload/users/50/', 'w' => 50, 'h' => 50],
    ];


    public static function Check_Soc_Reg($params) {
        $arr = Array('status'=>'ok');
        $params['email'] = strtolower($params['email']);

        if (!$params['email']) {
            $arr['status']='error';
            $arr['message'] = "Введите E-mail";
        } elseif (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            $arr['status']='error';
            $arr['message'] = "Введите корректный E-mail";
        }
        return $arr;
    }
    public static function Check_Reg($params) {
        $arr = Array('status'=>'ok');
        $params['email'] = strtolower($params['email']);

        if (!$params['name']) {
            $arr['status']='error';
            $arr['message'] = "Введите имя компании";
        }
        if (!$params['pass']) {
            $arr['status']='error';
            $arr['message'] = "Введите пароль";
        } elseif (!$params['pass1']) {
            $arr['status']='error';
            $arr['message'] = "Повторите пароль";
        } elseif ($params['pass']!=$params['pass1']) {
            $arr['status']='error';
            $arr['message'] = "Пароли не совпадают";
        }
        if (!$params['email']) {
            $arr['status']='error';
            $arr['message'] = "Введите логин";
        }
        
        if ($arr['status']=='ok') {
            $users = new Users();
            $users->GetOne([
                'email'=>cstr($params['email']),
                'del'=>0
            ]);
            if ($users->id) {
                $arr['status']='error';
                $arr['message']='Компания с таким логином уже существует. <br/>Введите другой';
            }
        }
        return $arr;
    }

    public static function GetPass($pass) {
        return md5($pass.'gfg5');
    }

    public static function login($email, $pass)
    {
        $arr = Array('status' => 'ok');
        $email = Functions::FixEmail($email);
        if ($email == '') {
            $arr['status'] = 'error';
            $arr['message'] = 'Введите логин';
        } elseif ($pass == '') {
            $arr['status'] = 'error';
            $arr['message'] = 'Введите пароль';
        } else {
            $user = new Users();
            $user->GetOne([
                'email'=>$email,
                'del'=>0
            ]);
            if ($user->id) {
                if ($user->data['pass'] == self::GetPass($pass)) {
                    $arr['message'] = 'Вы успешно вошли';
                } else {
                    $arr['status'] = 'error';
                    $arr['message'] = 'Неверный логин или пароль';
                }
            } else {
                $arr['status'] = 'error';
                $arr['message'] = 'Неверный логин или пароль';
            }
        }
        if ($arr['status'] == 'ok') {
            $user->AddAuth();
        }
        return $arr;
    }

    public static function logout()
    {
        global $db;
        mysqli_query($db,"delete from user_sessions where user_id=".\m\Users::authId());

        setcookie('user_id', '', time() - 3600, '/');
        setcookie('user_session_id', '', time() - 3600, '/');
        setcookie('user_session_pass', '', time() - 3600, '/');

    }

    function CheckAuth($user_id, $session_id, $ses_pass)
    {
        if (($user_id) && ($session_id) && ($ses_pass)) {
            $this->session = new UserSessions($session_id);
            if (($this->session->id)
                && ($this->session->data['ses_pass'] == $ses_pass)
                && ($user_id == $this->session->data['user_id']))
            {
                $this->Get($this->session->data['user_id']);
                if ($this->data['del']==1) {
                    $arr['status'] = 'error';
                    $arr['message'] = 'Your accaunt was deleted';
                    self::logout();
                } else {
                    $this->auth = true;
                    $this->type = ($this->data['active'] ? $this->data['type'] : 0);
                    if (time() - strtotime($this->data['date_active']) > 10) {
                        $this->Update(['date_active' => date('Y-m-d H:i:s')]);
                    }
                    if ($this->data['type'] == 0) {
                        $this->companies = new \s\User2User();
                        $this->companies->GetItems([
                            'to_user_id' => $this->id
                        ], [
                            'table_sql' => ", `users`",
                            'where_sql' => "AND `user2user`.`user_id`=`users`.`user_id` AND `users`.`type`=1 AND `users`.`del`=0 AND `users`.`active`=1"
                        ]);
                        if ($this->companies->count) {
                            foreach ($this->companies->items as $v) {
                                $this->companies_ids[] = $v['user_id'];
                            }
                        } else {
                            $this->companies_ids = [-1];
                        }
                    }
                    $arr['status'] = 'ok';
                }
            } else {
                $arr['status'] = 'error';
                $arr['message'] = 'Wrong session';
            }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Empty session';
        }

        if ($arr['status'] == 'error') {
            self::logout();
        }
        return $arr;
    }

    function AddAuth()
    {
        $arr = Array('status' => 'ok');
        if ($this->id) {
            $ses_pass = base64_encode(md5(strtolower($this->id . $this->data['pass']) . '*lxw*'));
            $this->session = new UserSessions();
            $this->session->Insert([
                'user_id'=>$this->id,
                'ip'=>$_SERVER['HTTP_X_REAL_IP'],
                'ses_pass'=>$ses_pass,
                'date'=>date('Y-m-d H:i:s')
            ], true);
            setcookie('user_id', $this->id, time() + 27 * 24 * 3600, '/');
            setcookie('user_session_pass', $ses_pass, time() + 27 * 24 * 3600, '/');
            setcookie('user_session_id', $this->session->id, time() + 27 * 24 * 3600, '/');
        } else {
            $arr['status'] = 'error';
            $arr['error'] = 'Пользователь не найден';
        }
        return $arr;
    }
    function Send_email_forgot() {
        $code = rand(10000, 999999);
        \s\Mail::Send_Mail($this->data['email'], 'Восстановление пароля', 'Код: '.$code);
        $this->Update(Array('forgot_code'=>$code));
    }
    public static function checkLoginName($email){
        global $db;
        if ($email){
            $sql = "SELECT `email` FROM `users` WHERE `email` = '".$email."'";
            $result = mysqli_query($db, $sql);
            if (mysqli_num_rows($result) > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }
}