<?php
namespace s;

class Vk {
    public static function GetClientID() {
        return 0;
    }
    public static function GetClientKey() {
        return '';
    }
    public static function authOpenAPIMember() {
        $session = array();
        $member = FALSE;
        $valid_keys = array('expire', 'mid', 'secret', 'sid', 'sig');
        $app_cookie = $_COOKIE['vk_app_'.Vk::GetClientID()];
        if ($app_cookie) {
            $session_data = explode ('&', $app_cookie, 10);
            foreach ($session_data as $pair) {
                list($key, $value) = explode('=', $pair, 2);
                if (empty($key) || empty($value) || !in_array($key, $valid_keys)) {
                    continue;
                }
                $session[$key] = $value;
            }
            foreach ($valid_keys as $key) {
                if (!isset($session[$key])) return $member;
            }
            ksort($session);

            $sign = '';
            foreach ($session as $key => $value) {
                if ($key != 'sig') {
                    $sign .= ($key.'='.$value);
                }
            }
            $sign .= Vk::GetClientKey();
            $sign = md5($sign);
            if ($session['sig'] == $sign && $session['expire'] > time()) {
                $member = array(
                    'id' => intval($session['mid']),
                    'secret' => $session['secret'],
                    'sid' => $session['sid']
                );
            }
        }
        return $member;
    }
    public static function Get($name, $params, $settings=Array()) {
        $arr = Array('status'=>'ok');
        if ($settings['proxy']) {
            $url = "http://seosev1.bget.ru/api.php?proxy_url=" . urlencode('https://api.vk.com/method/' . $name);
        } else {
            if ($settings['url']=='')
                $url = "https://api.vk.com/method/$name";
            else
                $url = $settings['url'];
        }
        $ch = curl_init();
        $a = false;
        $i=0;
        while ((!$a)&&($i<7)) {
            if ($i>0) {
                usleep(500000);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec( $ch );
            if (curl_errno($ch)) {
                $arr['status']='error';
                $arr['message']='connection error';
            } else {
                $arr['result'] = json_decode($response, true);
                if ((!is_array($arr['result']))||($arr['result']['error'])) {
                    $arr['status']='error';
                    $arr['message']='vk error';
                }
            }
            $i++;
        }
        curl_close($ch);
        return $arr;
    }
}