<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="viewport" content="width=device-width">
    <META NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW">
    <title><?=$this->page_title?></title>
    <link rel="icon" href="//<?=$_SERVER['HTTP_HOST']?>/images/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="//<?=$_SERVER['HTTP_HOST']?>/images/icon.png" type="image/x-icon">

    <!-- VK -->
    <script src="https://vk.com/js/api/openapi.js?139" type="text/javascript"></script>

    <!-- JQuery -->
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Fonts -->
    <link href='/css/fonts/Roboto.css' rel='stylesheet' type='text/css'>

    <!-- FontAwesome -->
    <link href="<?=s\Functions::GetFileSource('/plugins/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">

    <!-- select2-->
    <link href="<?= \s\Functions::GetFileSource('/plugins/select2/dist/css/select2.min.css')?>" rel="stylesheet" />
    <script type="text/javascript" src="/plugins/select2/dist/js/select2.min.js?i=2"></script>
    <script type="text/javascript" src="/plugins/select2/dist/js/i18n/ru.js"></script>

    <!-- Mask -->
    <script src="/js/_jquery.maskedinput-1.3.js?i=1" type="text/javascript"></script>

    <!-- JQuery UI -->
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/i18n/datepicker-ru.js?i=1"></script>
    <link href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css" rel="stylesheet" type="text/css">

    <!-- Customs CSS -->
    <link href="<?=s\Functions::GetFileSource('/css/index.css')?>" rel="stylesheet" type="text/css"/>
</head>
<body class="page-<?=$this->page['template']?> <?=($this->page['show_menu'] ? '' : 'centered')?>">
    <?php include AppDir.'/templates/includes/head.php'; ?>
    <div class="outer_wrapper">
        <div class="wrapper">
            <?php if (file_exists($this->template)) include $this->template; else echo $this->template; ?>
        </div>
    </div>
    <?php include AppDir.'/templates/includes/footer.php'; ?>
    <div class="overflow2menu"><div></div></div>
    
    <!-- JQuery Cookie -->
    <script type="text/javascript" src="<?=s\Functions::GetFileSource('/js/_jquery.cookie.js')?>"></script>
    
    <!-- FancyBox  -->
    <link href="/plugins/fancybox3/dist/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/plugins/fancybox3/dist/jquery.fancybox.min.js"></script>
    
    <!-- JQueryJSON -->
    <script type="text/javascript" src="<?=s\Functions::GetFileSource('/js/_jquery.json.js')?>"></script>
    
    <!-- JQueryForm -->
    <script src="<?=s\Functions::GetFileSource('/js/_jquery.form.js')?>"></script>

    <!-- Ripple -->
    <link rel="stylesheet" href="<?=s\Functions::GetFileSource('/plugins/ripple/src/css/lv-ripple.css')?>">
    <script src="<?=s\Functions::GetFileSource('/plugins/ripple/dist/js/lv-ripple.jquery.js')?>"></script>

    <!-- Customs JS -->
    <?php
    if (Local) {
        \s\Functions::getSrcScripts(1);
    } else {
        ?>
        <script src="<?=s\Functions::GetFileSource('/js/main.dist.js')?>" type="text/javascript"></script>
        <?php
    }
    ?>
    <link  href="/plugins/cropper/dist/cropper.min.css" rel="stylesheet">
    <script src="/plugins/cropper/dist/cropper.min.js"></script>



    <div class="overflow"></div>
    <?php include AppDir.'/templates/includes/metrica.php'; ?>
</body>
</html>