<?php
namespace s;

class Router
{
    public $urls = [];
    public $page = null;
    public $pages = [];
    public $page_title = 'MELZ';

    public $default_page2user = ['visitors', 'company', 'index', 'users'];
    public $template = '';

    function load($url)
    {
        global $user;

        if (!$url) {
            if ($user->auth) {
                $url = $this->default_page2user[$user->data['type']];
            } else {
                $url = 'index';
            }
        }

        $this->urls = explode('/', $url);
        $parent_id = 0;
        $isset = true;

        $page = new Pages();

        foreach ($this->urls as $index => $value) {
            if ($value) {
                $this->page = $page->GetByUrl($value, $parent_id);

                if ($this->page) {
                    $parent_id = $this->page['page_id'];
                    $this->pages[] = $this->page;
                    if ($this->page['child_id']==-1) {
                        break;
                    }
                } else {
                    $isset = false;
                    break;
                }
            }
        }

        if ($this->page['child_id']>0) {
            $this->page = $page->Get($this->page['child_id']);
            $this->pages[] = $this->page;
        }

        if (!$isset) {
            $this->page = $page->GetByUrl('404', 0);
            $this->pages = [$this->page];
            //header("HTTP/1.0 404 Not Found");
        }
        if ($this->page['user_auth'] == 1) {
            if ($user->auth) {
                if ($this->page['type'.$user->type] == 0) {
                    $this->page = $page->GetByUrl('403', 0);
                    $this->pages = [$this->page];
                }
            } else {
                $this->page = $page->GetByUrl('login', 0);
                $this->pages = [$this->page];
            }
        }
        if (($user->auth)&&
            (!in_array($this->page['url'], ['logout', 'profile']))&&
            (($user->data['active']==0)||
            (($user->type==0)&&($user->companies->count==0)))
        ) {
            $this->page = $page->GetByUrl('access', 0);
            $this->pages = [$this->page];
		} elseif (($user->auth) && (in_array($this->page['url'], ['cars'])) && ($user->data['show_car']==0))
        {
            $this->page = $page->GetByUrl('access', 0);
            $this->pages = [$this->page];
        }

        foreach ($this->pages as $p) {
            $this->page_title .= ' | ' . $p['title'];
        }
        //echo "<pre>";
        //print_r($this->pages);
    }
    public function run() {
        global $user, $router;
        include AppDir . '/controllers/index.php';
        foreach ($this->pages as $index => $value) {
            if ($value['controller']) {
                $controller = AppDir . '/controllers/' . $value['controller'] . '.php';
                if (file_exists($controller)) {
                    include $controller;
                }
            }
        }

        // $user2user = new User2User();
		// $user2user->GetItems(
		// 	[
		// 		'user_id' => 9
		// 	]
		// );

		// $rand = rand(15, 20);
		// foreach ($user2user->items as $to_user) {
		// 	if ($to_user['to_user_id'] == $user->data['user_id'] || $user->data['user_id'] == 9 ) {
		// 		sleep($rand);
		// 		break;
		// 	}
		// }

        if ($this->page['template']) {
            $this->template = AppDir . '/templates/' . $this->page['template'] . '.php';
        }
        if ($this->page['layout']) {
            $layout = AppDir . '/layouts/' . $this->page['layout'] . '.php';
            if (file_exists($layout)) {
                include $layout;
            }
        }
    }
}
