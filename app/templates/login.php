<div class="content">
    <div class="main_content">
        <div class="user_form user_login">
            <form method="POST" action="/ajax/users/login">
                <div class="title">Вход в личный кабинет</div>
                <div class="content_loader_hide">
                    <div class="inputs_login">
                        <div class="div_input"><input type="text" name="email" placeholder="Логин"/></div>
                        <div class="div_input"><input type="password" name="pass" placeholder="Пароль"/></div>
                        <div class="error" rel="main" style="margin-top: 10px;"></div>
                        <div class="btn block send_login">Войти</div>
                        <div class="term_privacy_text">
                            Нажимая на кнопку, вы даете
                            <a target="_blank" href="/confidential">согласие на обработку своих персональных данных</a>
                        </div>
                        <div class="soc_title" style="display: none;">Войти через</div>
                        <div class="soc_icons" style="display: none;">
                            <a class="item vk"><i class="fa fa-vk"></i></a><a class="item fb"><i class="fa fa-facebook"></i></a><a style="display: none;" class="item inst"><i class="fa fa-instagram"></i></a>
                            <div class="error" rel="soc"></div>
                        </div>
                    </div>
                    <div class="inputs_confirm" style="display: none;">
                        <input type="hidden" class="confirm_hash" value=""/>
                        <div class="div_input"><input type="email" class="confirm_email" placeholder="E-mail"/></div>
                        <div class="error" rel="confirm_send" style="margin-top: 10px;"></div>
                        <div class="btn block confirm_send">Далее</div>
                        <div class="step_confirm_2" style="display:none;">
                            <div class="div_text">На ваш E-mail оправлено письмо с кодом</div>
                            <div class="div_input"><input type="text" class="confirm_code" placeholder="Введите код из письма"/></div>
                            <div class="error" rel="confirm_login" style="display: block;margin-top: 20px;"></div>
                            <div class="btn block confirm_login">Подтвердить</div>
                        </div>
                        <div class="term_privacy_text">
                            Нажимая на кнопку, вы даете
                            <a target="_blank" href="/confidential">согласие на обработку своих персональных данных</a>
                        </div>
                        <a href="/lk" class="link_login">Вход</a>
                    </div>
                    <a href="/reg" class="register">Регистрация</a>
                    <a href="/forgot" style="display: none;" class="forgot">Забыли пароль?</a>
                </div>
                <div class="loader_login_content"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
            </form>
        </div>
    </div>
</div>