function refresh() {
    let finder = $('.items_finder');
    this.items = $('.LineItems .items');
    this.iload = $('.iload');
    let find = {};
    find.text = finder.find('input.text').val();
    find.company_user_id = finder.find('select[name="company_user_id"]').val();
    find.status = finder.find('.item_finder_tabs .tab.active').attr('rel');
    find.type = finder.find('.item_finder_types .tab.active').attr('rel');

    if (find.type === 'car') {
        $.post('/ajax/shield/get_items', {
            find: find,
            i: this.i
        }, (r) => {
            let a = jQuery.parseJSON(r);
            if (a['status'] === 'ok') {
                if (parseInt(a['start'])===0) {
                    console.log('!');
                    this.items.html('');
                }
                this.items.html(a['html']);
            }
        });
    }
}

$(document).ready(function(){
    setInterval('refresh()', (60 * 1000));
});
