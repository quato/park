<div class="LineItems Shield">
    <div class="items_finder grouped">
        <div class="items_finder_group _big">
            <div class="item_finder text_search" style="width: 200px;">
                <input value="<?=cstr($_GET['text'])?>" class="text" placeholder="Поиск постетителей" type="text"/>
                <div class="find_btn"><i class="fa fa-search" aria-hidden="true"></i></div>
            </div>
            <div class="item_finder not_grow">
                <a href="/logout" class="form_button">Закрыть смену</a>
            </div>
        </div>
        <div class="items_finder_group _mini">
            <div class="item_finder not_grow" style="display: none;">
                <select style="min-width:250px;" name="company_user_id" data-placeholder="Выбрать компанию" data-params='{"type":1}'">

                </select>
            </div>
            <div class="item_finder not_grow" style="flex-shrink: 1;">
                <div class="item_finder_tabs">
                    <div class="mini tab active" rel="0">Все</div>
                    <div class="mini tab" rel="none">Вошли</div>
                    <div class="mini tab" rel="in">В здании</div>
                    <div class="mini tab" rel="out">Вышли</div>
                </div>
                <div class="item_finder_types">
                    <div class="mini tab active" rel="people">Люди</div>
                    <div class="mini tab" rel="car">Авто</div>
                </div>
            </div>
        </div>
    </div>
    <h2>Результаты поиска:</h2>
    <div class="items"></div>
    <div class="iload"></div>
</div>
<script src="/js/refresh.car.js"></script>