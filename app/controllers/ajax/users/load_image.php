<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
}
$rez = \s\Forms::LoadFromTmp($_POST['img_crop'], $user->imageCfg(), $user->data['image0'], false);
$user->Update([
    'image0'=>$rez['filename']
]);
$tmp_dir = $_SERVER['DOCUMENT_ROOT'].'/tmp/';
$img_crop = $tmp_dir.$_POST['img_crop'];
$img_full = $tmp_dir.$_POST['img_full'];
if (file_exists($img_crop)) {
    unlink($img_crop);
}
if (file_exists($img_full)) {
    unlink($img_full);
}