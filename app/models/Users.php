<?php
namespace m;
/**
 * Работа с клиентами
 * Class Users
 */
class Users extends \m\Models {
    public $table_name='users';
    public $id_name='user_id';
    public $options = [
        'order'=>'`users`.`user_id` DESC'
    ];

    function FormatParams($params) {
        if (isset($params['name'])) $set['name']=cstr($params['name']);
        if (isset($params['fename'])) $set['fename']=cstr($params['fename']);
        if (isset($params['pass'])) $set['pass']=cstr(\s\Users::GetPass($params['pass']));
        if (isset($params['phone'])) $set['phone']=cstr(\s\Functions::FixPhone($params['phone']));
        if (isset($params['email'])) $set['email']=cstr(strtolower($params['email']));
        if (isset($params['forgot_code'])) $set['forgot_code']=cstr($params['forgot_code']);
        if (isset($params['vk_id'])) $set['vk_id']=cstr($params['vk_id']);
        if (isset($params['fb_id'])) $set['fb_id']=cstr($params['fb_id']);
        if (isset($params['fb_token'])) $set['fb_token']=cstr($params['fb_token']);
        if (isset($params['image0'])) $set['image0']=cstr($params['image0']);

        return $params;
    }

    function AddParams($params) {
        $params['date'] = date('Y-m-d H:i:s');
        return $params;
    }

    function authId()
    {
        return $_COOKIE["user_id"];
    }
}
