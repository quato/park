<?php
namespace s;

class Get {
    public $cookie;
    public $proxy;
    public $id;
    
    function send($url, $post=[], $params=[]) {
        $arr=Array('status'=>'ok');
        $ch = curl_init();
        if (count($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $curl_header=Array();

        if ($params['host']) {
            $curl_header[]='Host: '.$params['host'];
        }
        if ($params['HTTP_X_REQUESTED_WITH']) {
            $curl_header[]="X-Requested-With: ".$params['HTTP_X_REQUESTED_WITH'];
        }
        if ($params['Content-Type']) {
            $curl_header[]="Content-Type: ".$params['Content-Type'];
        }
        if ($params['X-CSRF-Token']) {
            $curl_header[]="X-CSRF-Token: ".$params['X-CSRF-Token'];
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        if ($this->id) {
            $win = 64;
            $ver_win = '10.0';
            if ($this->id % 2 == 0) {
                $win = 32;
                $ver_win = '7.0';
            }
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT '.$ver_win.'; Win'.$win.'; x'.$win.'; rv:58.0) Gecko/2010' . $this->id . ' Firefox/58.0');
        } else {
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0');
        }
        
        if (($this->proxy)&&($this->proxy->data['ip'])) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy->data['ip'].':'.$this->proxy->data['port']);
            if ($this->proxy->data['user']) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy->data['user'] . ':' . $this->proxy->data['pass']);
            }
        }

        if ($this->cookie) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie);
        }

        if ($params['cookie_new']) {
            curl_setopt($ch, CURLOPT_COOKIE, $params['cookie_new']);
        }

        if (!isset($params['timeout'])) $params['timeout']=7;
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $params['timeout']);
        curl_setopt($ch, CURLOPT_TIMEOUT, $params['timeout']);
        
        if ($params['referer']) {
            curl_setopt($ch, CURLOPT_REFERER, $params['referer']);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

        if ($params['ssl4']) {
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
        }
        if ($params['get_file']) {
            $filename = time() . rand(1, 999) . '.jpg';
            $file_full = $_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $filename;
            $fp = fopen($file_full, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            $response = curl_exec($ch);
            fclose($fp);
            $arr['file']=$file_full;
        } else {
            $response = curl_exec($ch);
        }
        if(curl_errno($ch)) {
            $arr['status']='error';
            $arr['message']=curl_error($ch);
            $arr['error']=curl_errno($ch);
        }
        //$sent_headers = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        $arr['html'] = $response;
        curl_close($ch);
        return $arr;
    }
}