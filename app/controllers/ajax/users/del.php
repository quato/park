<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->type!=3) {
    $arr['status'] = 'error';
    $arr['message'] = 'Нет доступа';
}
if ($arr['status'] == 'ok') {
    $u = new \s\Users($_POST['user_id']);
    if ($u->id) {
        $u->Disable();
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Пользователь не найден';
    }
}