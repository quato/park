<h1 class="page_title">Профиль</h1>

<?php
/*
$dblocation = "localhost";
$dbname = "u0386027_melz";
$dbuser = "root";
$dbpasswd = "root";

global $db;

$dbcnx = mysqli_connect($dblocation, $dbuser, $dbpasswd, $dbname);
if (!$dbcnx)
{
echo "<p>К сожалению, не доступен сервер mySQL</p>";
exit();
}
if (!mysqli_select_db($db,$dbname) )
{
echo "<p>К сожалению, не доступна база данных</p>";
exit();
}
$result = mysqli_query($dbcnx,"SELECT * from users");
while ($row = mysqli_fetch_row($result)) {
    echo $row[0].' '.$row[1].' '.$row[2].' '.$row[3].' '.$row[4].'<br>';
}*/
?>



<div class="UserProfile">
    <div class="image4crop" data-name="avatar" data-image="<?=($user->data['image0'] ? $user->ImageCfg()[1]['url'].$user->data['image0'] : '')?>"></div>
    <div class="btn block change_pass" style="display: none;">Изменить пароль</div>
    <div class="frow">
        <div class="ititle">Имя</div>
        <?php if($user->type != 3) { $disabled = "disabled"; }else{ $disabled = "";} ?>
        <input class="form_control" type="text" name="name" <?= $disabled ?> value="<?=$user->data['name']?>"/>
        <div style="clear:both;"></div>
    </div>
    <div class="frow">
        <div class="ititle">Логин</div>
        <input class="form_control" disabled="disabled" type="text" name="email" value="<?=$user->data['email']?>"/>
        <div style="clear:both;"></div>
    </div>
    <div class="error"></div>
    <div class="btn block save_profile">Сохранить</div>
    <div class="result_form"></div>
    <div style="clear:both;"></div>
</div>
<div class="window mini" rel="change_pass">
    <div class="inner_content">
        <div class="close"><div class="iclose"></div></div>
        <div class="icontent">
            <h2>Изменить пароль</h2>
            <div class="change_pass_form card_form">
                <div class="form_item">
                    <label class="form_control">Введите новый пароль</label>
                    <input type="password" placeholder="" class="form_control big change_pass_val"/>
                </div>
                <div class="error" rel="ok"></div>
                <div class="btn block change_pass_ok">Применить</div>
            </div>
            <div class="change_pass_text">
                Ваш пароль успешно изменен
            </div>
        </div>
    </div>
</div>