<?php
$host = $_SERVER['HTTP_HOST'];
$main_email = 'admin@'.$host;
?>
<h2>Политика конфиденциальности</h2>

<div>
    <p>Настоящая Политика конфиденциальности персональной информации (далее — <b>Политика</b>) действует в отношении
        всей
        информации, которую сайт <?=$host?>
        и/или его аффилированные лица, могут получить о пользователе во время использования им сайта
        <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a>.</p>
    <p>Использование сайта <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a> означает безоговорочное согласие
        пользователя с настоящей <b>Политикой</b> и
        указанными в ней условиями обработки его персональной информации; в случае несогласия с этими условиями
        пользователь должен воздержаться от использования данного ресурса.</p>
    <p><b>1.</b> Персональная информация пользователей, которую получает и обрабатывает сайт <a href="/"
                                                                                                title="<?=$host?>"><?= ucfirst($host) ?></a>
    </p>
    <p><b>1.1.</b> В рамках настоящей <b>Политики</b> под «персональной информацией пользователя» понимаются:</p>
    <p><b>1.1.1.</b> Персональная информация, которую пользователь предоставляет о себе самостоятельно при оставлении
        заявки,
        совершении покупки, регистрации (создании учётной записи) или в ином процессе использования сайта.</p>
    <p><b>1.1.2.</b> Данные, которые автоматически передаются сайтом <a href="/"
                                                                        title="<?=$host?>"><?= ucfirst($host) ?></a> в
        процессе его использования с помощью
        установленного на устройстве пользователя программного обеспечения, в том числе IP-адрес, информация из cookie,
        информация о браузере пользователя (или иной программе, с помощью которой осуществляется доступ к сайту), время
        доступа, адрес запрашиваемой страницы.</p>
    <p><b>1.1.3.</b> Данные, которые предоставляются сайту, в целях осуществления оказания услуг и/или продаже товара
        и/или
        предоставления иных ценностей для посетителей сайта, в соответствии с деятельностью настоящего ресурса:</p>
    <p>• фамилия<br/>
        • имя<br/>
        • отчество<br/>
        • электронная почта<br/>
        • номер телефона<br/>
        • фотография<br/>
        • ссылка на персональный сайт или соцсети<br/>
        • ip адрес<br/>
        • Cookie</p>
    <p><b>1.2.</b> Настоящая <b>Политика</b> применима только к сайту <a href="/"
                                                                         title="<?=$host?>"><?= ucfirst($host) ?></a> и
        не
        контролирует и не несет ответственность за
        сайты третьих лиц, на которые пользователь может перейти по ссылкам, доступным на сайте <a href="/"
                                                                                                   title="<?=$host?>"><?= ucfirst($host) ?></a>.
        На таких
        сайтах у пользователя может собираться или запрашиваться иная персональная информация, а также могут совершаться
        иные действия.</p>
    <p><b>1.3.</b> Сайт в общем случае не проверяет достоверность персональной информации, предоставляемой
        пользователями, и не
        осуществляет контроль за их дееспособностью. Однако сайт
        <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a> исходит из того, что пользователь предоставляет
        достоверную и достаточную персональную информацию
        по вопросам, предлагаемым в формах настоящего ресурса, и поддерживает эту информацию в актуальном состоянии.</p>
    <p><b>2.</b> Цели сбора и обработки персональной информации пользователей</p>
    <p><b>2.1.</b> Сайт собирает и хранит только те персональные данные, которые необходимы для оказания услуг и/или
        продаже
        товара и/или предоставления иных ценностей для посетителей сайта <a href="/"
                                                                            title="<?=$host?>"><?= ucfirst($host) ?></a>.
    </p>
    <p><b>2.2.</b> Персональную информацию пользователя можно использовать в следующих целях:</p>
    <p><b>2.2.1</b> Идентификация стороны в рамках соглашений и договоров с сайтом</p>
    <p><b>2.2.2</b> Предоставление пользователю персонализированных услуг и сервисов, товаров и иных ценностей</p>
    <p><b>2.2.3</b> Связь с пользователем, в том числе направление уведомлений, запросов и информации, касающихся
        использования
        сайта, оказания услуг, а также обработка запросов и заявок от пользователя</p>
    <p><b>2.2.4</b> Улучшение качества сайта, удобства его использования, разработка новых товаров и услуг</p>
    <p><b>2.2.5</b> Таргетирование рекламных материалов</p>
    <p><b>2.2.6</b> Проведение статистических и иных исследований на основе предоставленных данных</p>
    <p><b>2.2.7</b> Заключения, исполнения и прекращения гражданско-правовых договоров с физическими, юридическими
        лицами,
        индивидуальными предпринимателями и иными лицами, в случаях, предусмотренных действующим законодательством и/или
        Уставом предприятия</p>
    <p><b>3.</b> Условия обработки персональной информации пользователя и её передачи третьим лицам</p>
    <p><b>3.1.</b> Сайт <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a> хранит персональную информацию
        пользователей в
        соответствии с внутренними регламентами
        конкретных сервисов.</p>
    <p><b>3.2.</b> В отношении персональной информации пользователя сохраняется ее конфиденциальность, кроме случаев
        добровольного
        предоставления пользователем информации о себе для общего доступа неограниченному кругу лиц.</p>
    <p><b>3.3.</b> Сайт <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a> вправе передать персональную информацию
        пользователя третьим лицам в следующих случаях:</p>
    <p><b>3.3.1.</b> Пользователь выразил свое согласие на такие действия, путем согласия выразившегося в предоставлении
        таких
        данных;</p>
    <p><b>3.3.2.</b> Передача необходима в рамках использования пользователем определенного сайта <a href="/"
                                                                                                     title="<?=$host?>"><?= ucfirst($host) ?></a>,
        либо для
        предоставления товаров и/или оказания услуги пользователю;</p>
    <p><b>3.3.3.</b> Передача предусмотрена российским или иным применимым законодательством в рамках установленной
        законодательством процедуры;</p>
    <p><b>3.3.4.</b> В целях обеспечения возможности защиты прав и законных интересов сайта
        <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a> или третьих лиц в случаях, когда пользователь нарушает
        Пользовательское соглашение сайта
        <a href="/" title="<?=$host?>"><?= ucfirst($host) ?></a>.</p>
    <p><b>3.4.</b> При обработке персональных данных пользователей сайт <a href="/"
                                                                           title="<?=$host?>"><?= ucfirst($host) ?></a>
        руководствуется Федеральным законом РФ
        «О персональных данных».</p>
    <p><b>4.</b> Изменение пользователем персональной информации</p>
    <p><b>4.1.</b> Пользователь может в любой момент изменить (обновить, дополнить) предоставленную им персональную
        информацию
        или
        её часть, а также параметры её конфиденциальности, оставив заявление в адрес администрации сайта следующим
        способом:</p>
    <p>• Email: <a href="mailto:<?= $main_email ?>"><?= $main_email ?></a></p>
    <p><b>4.2.</b> Пользователь может в любой момент, отозвать свое согласие на обработку персональных данных, оставив
        заявление в
        адрес администрации сайта следующим способом:</p>
    <p>• Email: <a href="mailto:<?= $main_email ?>"><?= $main_email ?></a></p>
    <p><b>5.</b> Меры, применяемые для защиты персональной информации пользователей</p>
    <p>Сайт принимает необходимые и достаточные организационные и технические меры для защиты персональной информации
        пользователя от неправомерного или случайного доступа, уничтожения, изменения, блокирования, копирования,
        распространения, а также от иных неправомерных действий с ней третьих лиц.</p>
    <p><b>6.</b> Изменение <b>Политики конфиденциальности</b>. Применимое
        законодательство</p>
    <p><b>6.1.</b> Сайт имеет право вносить изменения в настоящую <b>Политику конфиденциальности</b>. При внесении
        изменений в
        актуальной
        редакции указывается дата последнего обновления. Новая редакция <b>Политики</b> вступает в силу с момента ее
        размещения,
        если иное не предусмотрено новой редакцией <b>Политики</b>.</p>

    <p><b>6.2.</b> К настоящей <b>Политике</b> и отношениям между пользователем и Сайтом, возникающим в связи с
        применением <b>Политики
            конфиденциальности</b>, подлежит применению право Российской Федерации.</p>
    <p><b>7.</b> Обратная связь. Вопросы и предложения</p>
    <p><b>7.1.</b> Все предложения или вопросы по поводу настоящей <b>Политики</b> следует направлять следующим
        способом:</p>
    <p>• Телефон: <?= $main_phone ?></p>
    <p>• Email: <a href="mailto:<?= $main_email ?>"><?= $main_email ?></a></p>
</div>