<?php
$arr = s\Users::Check_Reg($_POST);
if ($arr['status']=='ok') {
    //$email = s\Functions::FixEmail($_POST['email']);
    //$code = s\EmailCodes::GetCode($_POST['hash'], $email);
    //$arr['hash'] = $code['hash'];
    //s\Mail::Send_Mail($email, 'Регистрация', 'Код: '.$code['code']);
    $params = [
        'name'=>$_POST['name'],
        'email'=>cstr($_POST['email']),
        'type'=>1,
        'active'=>0,
        'pass'=>\s\Users::GetPass($_POST['pass'])
    ];
    $users = new \s\Users();
    $users->GetOne([
        'email'=>$params['email'],
        'del'=>0
    ]);
    if ($users->id) {
        $arr['status']='error';
        $arr['message']='Компания с таким логином уже существует';
    } else {
        $users->Insert($params, true);
        $users->AddAuth();
        $arr['message'] = 'Вы успешно зарегистрировались';
    }
}