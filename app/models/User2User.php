<?php
namespace m;

class User2User extends Models {
    public $table_name='user2user';
    public $id_name='user_id';
    public $id_name2='to_user_id';
    public $options = [
        'order'=>'`user2user`.`user_id` ASC'
    ];
}