<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} else {
    if ($user->type!=3) {
        if ($user->type==0)
            $_POST['company_user_id'] = $user->companies_ids[0];
        else
            $_POST['company_user_id'] = $user->id;
        if (!$_POST['company_user_id']) {
            $arr['status'] = 'error';
            $arr['message'] = 'Выберите компанию';
        }
    }
    if ($_POST['date']) {
        if (strtotime($_POST['date'])<strtotime(date('Y-m-d'))) {
            $arr['status'] = 'error';
            $arr['message'] = 'Нельзя выбрать дату которая уже прошла';
        }
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите дату';
    }
}
if ($arr['status'] == 'ok') {
    if ((isset($_FILES['list']['tmp_name']))&&($_FILES['list']['tmp_name'] != '')) {
        $arr = \s\Forms::CheckFile($_FILES['list']['tmp_name'], ['txt', 'doc']);
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Файл не загружен. Возможно его размер больше ' . ini_get('upload_max_filesize');
    }
}
if ($arr['status'] == 'ok') {
    $dates = [];
    if ($_POST['type_date'] == 1) {
        $date = strtotime($_POST['date_from']);
        $date_to = strtotime($_POST['date_to']);
        if ($date > $date_to) {
            $arr['status'] = 'error';
            $arr['message'] = 'Дата начала периода не может быть меньше даты конца';
        } else {
            while ($date <= $date_to) {
                $dates[] = date('Y-m-d', $date);
                $date += 3600 * 24;
            }
        }
    } else {
        $dates = [date('Y-m-d', strtotime($_POST['date']))];
    }
}
if ($arr['status'] == 'ok') {
    $c = new \s\Visitors();
    if ($arr['type']=='txt') {
        $fd = fopen($_FILES['list']['tmp_name'], 'r');
        if ($fd) {
            while (!feof($fd)) {
                $str = fgets($fd);
                $str = mb_convert_encoding($str, 'UTF-8', "cp1251");
                $data = explode(' ', $str);
                foreach ($dates as $date) {
                    $params = [
                        'name' => $data[0],
                        'fename' => $data[1],
                        'company_user_id' => intval($_POST['company_user_id']),
                        'car_brand_id' => intval($data[2]),
                        'car_number' => cstr($data[3]),
                        'passengers' => cstr($data[4]),
                        'date' => $date,
                        'date_add' => date('Y-m-d H:i:s')
                    ];
                    $c->GetOne($params);
                    if (!$c->id) {
                        $c->Insert($params);
                    }
                }
            }
            fclose($fd);
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Ошибка открытие файла';
        }
    } else {
        require_once AppDir.'/includes/PhpSpreadsheet/src/Bootstrap.php';

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['list']['tmp_name']);
        $sheet = $spreadsheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        print_r($sheet);
        for ($row = 2; $row <= $highestRow; ++$row) {
            $name = $sheet->getCell('A'.$row)->getValue();
            $fename = $sheet->getCell('B'.$row)->getValue();
            $car_brand_id = $sheet->getCell('C'.$row)->getValue();
            $weight = $sheet->getCell('D'.$row)->getValue();
            $car_number = $sheet->getCell('E'.$row)->getValue();
            $passengers = $sheet->getCell('F'.$row)->getValue();
            echo $car_brand_id;
            foreach ($dates as $date) {
                $params = [
                    'name' => $name,
                    'fename' => $fename,
                    'company_user_id' => intval($_POST['company_user_id']),
                    'car_brand_id' => intval($car_brand_id),
                    'weight' => intval($weight),
                    'car_number' => cstr($car_number),
                    'passengers' => cstr($passengers),
                    'date' => $date,
                    'date_add' => date('Y-m-d H:i:s')
                ];
                $c->GetOne($params);
//                if (!$c->id) {
                    $c->Insert($params);
//                }
            }
        }
    }
}