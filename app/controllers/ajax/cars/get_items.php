<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
}
if ($arr['status'] == 'ok') {
    $countItems = 30;
    ob_start();
    $view['visitors'] = new \s\Visitors();
    $weight = array(0=>'До 3.5т', 1=>'От 3.5т до 10т', 2=>'Больше 10т');
    $params = [];
    $options = [
        'field_sql' => "`visitors`.*, `users`.`name` as `company_name`, `car_brand`.*",
        'table_sql' => "LEFT JOIN `users` ON `users`.`user_id`=`visitors`.`company_user_id` LEFT JOIN `car_brand` ON `visitors`.`car_brand_id`=`car_brand`.`car_brand_id`",
        'limit_count' => $countItems,
        'limit_from' => $_POST['i']
    ];
    if ($_POST['find']['text']) {
        $str = cstr($_POST['find']['text']);
        $w = '';
        $a = explode(" ", $str);

        if (count($a) == 2) {
            $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%' AND `visitors`.`fename` LIKE '" . $a[1] . "%') 
            OR (`visitors`.`name` LIKE '" . $a[1] . "%' AND `visitors`.`fename` LIKE '" . $a[0] . "%')";
        } else {
            foreach ($a as $str) {
                $int = intval($str);
                $w .= ($int ? " OR `visitors`.`visitor_id`=$int" : "") . " OR `visitors`.`name` LIKE '$str%' OR `visitors`.`fename` LIKE '$str%'";
            }
        }
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($_POST['find']['car_brand'])
    {
        $params['car_brand_id'] = intval($_POST['find']['car_brand']);
    }
    if ($_POST['find']['car_number'])
    {
        $str = cstr($_POST['find']['car_number']);
        $w = '';
        $w .= " OR (`visitors`.`car_number` LIKE '" . $str . "%') ";
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($_POST['find']['admin_name'])
    {
        $str = cstr($_POST['find']['admin_name']);
        $w = '';
        $a = explode(" ", $str);
        $w .= " OR (`visitors`.`admin_name` LIKE '" . $str . "%') ";
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($_POST['find']['weight']!='none')
    {
        $options['where_sql'] .= " AND `visitors`.`weight`=" . intval($_POST['find']['weight']);
    }
    $options['where_sql'] .= " AND `visitors`.`car_number`!=''";
    if ($user->type == 1) {
        $options['where_sql'] .= " AND `visitors`.`company_user_id`=" . $user->id;
    } elseif ($user->type == 0) {
        $options['where_sql'] .= " AND `visitors`.`company_user_id` IN (" . implode(',', $user->companies_ids) . ")";
    }
    if ($_POST['find']['company_user_id']) {
        $params['company_user_id'] = intval($_POST['find']['company_user_id']);
    }

    if ($_POST['find']['status']) {

        if ($_POST['find']['status']=='none') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NULL";
        } elseif ($_POST['find']['status']=='inner') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL";
        } elseif ($_POST['find']['status']=='in') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL";
        } elseif ($_POST['find']['status']=='out') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NOT NULL";
        }
    }

    if ($_POST['find']['time_out'])
    {
        $options['where_sql'] .= " AND HOUR(`date_out`)>0 AND HOUR(`date_out`)<6";
    }
    if ($_POST['find']['date_from']) {
        $options['where_sql'] .= " AND `visitors`.`date` >= '".date('Y-m-d', strtotime($_POST['find']['date_from']))."'";
    }
    if ($_POST['find']['date_to']) {
        $options['where_sql'] .= " AND `visitors`.`date` <= '".date('Y-m-d', strtotime($_POST['find']['date_to']))."'";
    }
    $rez = $view['visitors']->GetItems($params, $options);
    $arr['i'] = $_POST['i'] + $countItems;
    $arr['count'] = $view['visitors']->count;
    foreach ($view['visitors']->items as $item) {
        ?>
        <tr class="ripple" data-id="<?= $item['visitor_id'] ?>" data-del="<?=$item['del']?>">
            <td><?= date('d.m.Y', strtotime($item['date'])) ?></td>
            <td><?= $item['company_name'] ?></td>
            <td><?= $item['admin_name'] ?></td>
            <td><?= $item['name'] ?> <?= $item['fename'] ?></td>
            <td><?= $item['car_brand'] ?></td>
            <td><?= $item['car_number'] ?></td>
            <td><?= $weight[$item['weight']] ?></td>
            <td><?= $item['passengers'] ?></td>
            <td><?= date('d.m.Y', strtotime($item['date_add'])) ?></td>
            <td><?= ($item['date_in'] ? date('d.m.Y', strtotime($item['date_in'])) : '') ?></td>
            <td><?= ($item['date_in'] ? date('H:i:s', strtotime($item['date_in'])) : '') ?></td>
            <td><?= ($item['date_out'] ? date('d.m.Y', strtotime($item['date_out'])) : '') ?></td>
            <td><?= ($item['date_out'] ? date('H:i:s', strtotime($item['date_out'])) : '') ?></td>
        </tr>
        <?php
    }
    $arr['html'] = ob_get_contents();
    ob_clean();
   
}