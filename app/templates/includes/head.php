<header>
    <div class="in_header">
        <div class="show_menu"><div class="nav-icon"><span></span></div></div>
        <a class="logo ripple" href="/">
            <div class="name">MELZ.info</div>
            <div class="title">Онлайн-система пропусков</div>
        </a>
        <?php if($user->auth) { ?>
            <a class="user ripple" href="/profile">
                <div class="info">
                    <div class="position"><?=$user->types[$user->data['type']]?></div>
                    <div class="name"><?=$user->data['name']?></div>
                </div>
                <div class="avatar"><div style="<?=($user->data['image0'] ? "background-image: url('{$user->ImageCfg()[2]['url']}{$user->data['image0']}');" : '')?>" class="img"></div></div>
            </a>
        <?php } else { ?>
            <div class="user">
                <a href="/" class="login">
                    Войти
                </a>
            </div>
        <?php } ?>
    </div>
</header>
<div class="overflow_menu"></div>
<div class="main_menu" >
    <div class="items">
        <?php
        if ($user->auth) {
            foreach ($view['menu_pages']->items as $item) {
                if ($user->data['show_car'] == 0 && $item['url'] == 'cars') {

                } else {
					?>
                    <a href="/<?= $item['url'] ?>"
                       class="item ripple <?= ($router->page['page_id'] == $item['page_id'] ? 'active' : '') ?>">
                        <i class="icon icon-<?= $item['icon'] ?>"></i>
                        <div class="title nomargin"><?= $item['title'] ?></div>
                    </a>
					<?php
                }
            }
        } else {
            ?>
            <a href="/"
               class="item ripple">
                <div class="title nomargin">Вход</div>
            </a>
            <?php
        }
        ?>
    </div>
    <div class="menu_footer">
        <?php
        foreach ($view['footer_pages']->items as $item) {
            ?>
            <a href="/<?=$item['url']?>" class="item"><?=$item['title']?></a>
            <?php
        }
        ?>
    </div>
</div>