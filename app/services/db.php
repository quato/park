<?php
namespace s;

class db {
    private $data = [
        'main'=> [
            'host' => "localhost",
            /*'user' => "u0386027_melz",*/
            /*'pass' => "U5x7R0s4",*/
            'user' => "root",
            'pass' => "root",
            'db_name' => "u0386027_melz"
        ],
        'local'=> [
            'host' => "localhost",
            /*'user' => "u0386027_melz",
            'pass' => "U5x7R0s4",*/
            'user' => "root",
            'pass' => "root",
            'db_name' => "u0386027_melz"
        ]
    ];
    public function connect() {
        $type = strtolower('local');
        $data = $this->data[$type];
        $db = mysqli_connect($data['host'], $data['user'], $data['pass'], $data['db_name']);
        mysqli_query($db, "set character_set_client='utf8mb4'");
        mysqli_query($db, "set character_set_results='utf8mb4'");
        mysqli_query($db, "set collation_connection='utf8mb4_unicode_ci'");
        mysqli_query($db, "set names 'utf8mb4'");
        return $db;
    }
}