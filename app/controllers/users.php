<?php
$view['status'] = 'ok';
if ($_GET['id']) {
    $view['user'] = new \s\Users();
    $view['user2user'] = new \s\User2User();
    if (is_numeric($_GET['id'])) {
        $view['user']->Get($_GET['id']);
        if (!$view['user']->id) {
            $view['status'] = 'error';
            $view['message'] = 'Пользователь не найден';
        } else {
            $view['title'] = $view['user']->data['name'];
            $r = $view['user2user']->GetItems([
                'to_user_id'=>$view['user']->id
            ], [
                'table_sql'=>", `users`",
                'where_sql'=>"AND `user2user`.`user_id`=`users`.`user_id` AND `users`.`type`=1 AND `users`.`del`=0"
            ]);
        }
    } else {
        $view['title'] = 'Добавить';
        $view['user']->data['active']=1;
        $view['user']->data['type']=1;
    }
}