class CarVisitorsAdmin_Edit {
    save() {
        if (!this.btn_save.hasClass('load')) {
            this.btn_save.formbutton('start');
            this.error.html('');
            $.post('/ajax/visitors/save', this.el.find('input, select, textarea').serializeArray(), (r)=>{
                let a = jQuery.parseJSON(r);
                if (a.status==='ok') {
                    location.href = '?';
                } else {
                    this.btn_save.formbutton('stop');
                    this.error.html(a.message);
                }
            });
        }
    }
    del() {
        if ((!this.btn_del.hasClass('load'))&&(confirm(this.btn_del.find('.t').text().trim()+' посетителя?'))) {
            this.btn_del.formbutton('start');
            this.error.html('');
            $.post('/ajax/visitors/del', {
                visitor_id: this.el.find('input[name="visitor_id"]').val(),
                del: (parseInt(this.btn_del.data('del'))===1 ? 0 : 1)
            }, (r)=>{
                let a = jQuery.parseJSON(r);
                if (a.status==='ok') {
                    location.href = '?';
                } else {
                    this.btn_del.formbutton('stop');
                    this.error.html(a.message);
                }
            });
        }
    }
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.error = this.el.find('.error');
            this.btn_save = this.el.find('.form_button.save');
            this.btn_del = this.el.find('.form_button.delete');
            let Select2 = new CarSelect2(this.el.find('select[name="car_brand"]'));
            this.btn_save.on('click', ()=>{
                this.save();
            });
            this.btn_del.on('click', ()=>{
                this.del();
            });
        }
    }
}
class CarVisitorsAdmin {
    load_items() {
        if (!this.loading) {
            this.loading = true;
            this.iload.show();
            $.post('/ajax/cars/get_items', {
                find: this.find_data,
                i: this.i
            }, (r) => {
                let a = jQuery.parseJSON(r);
                if (a['status'] === 'ok') {
                    this.items.append(a['html']);
                    let non_rel = this.items.find('.item[rel="0"]');
                    if (non_rel.length>0) {
                        non_rel.ripple();
                        non_rel.attr('rel', 1);
                    }
                    this.el.trigger('resize');
                    this.i = a['i'];
                    this.hasItems = (this.i < a['count']);
                    this.loading = false;
                    this.iload.hide();
                    if ((this.hasItems) && (parseInt(this.el.height()) < parseInt($(window).height()) + 200)) {
                        this.load_items();
                    }
                }
            });
        }
    }
    load_list() {
        if (!this.btn_list.hasClass('load')) {
            let form_data = new FormData;
            form_data.append('list', this.wndVisitorList.find('input[type="file"]').prop('files')[0]);
            form_data.append('date', this.wndVisitorList.find('input[name="date"]').val());
            form_data.append('type_date', this.wndVisitorList.find('input[name="type_date"]:checked').val());
            form_data.append('date_from', this.wndVisitorList.find('input[name="date_from"]').val());
            form_data.append('date_to', this.wndVisitorList.find('input[name="date_to"]').val());
            form_data.append('car_brand', this.wndVisitorList.find('input[name="car_brand"]').val());
            form_data.append('car_number', this.wndVisitorList.find('input[name="car_number"]').val());
            form_data.append('weight', this.wndVisitorList.find('select[name="weight"]').val());
            form_data.append('time_out', this.wndVisitorList.find('select[name="time_out"]').val());
            form_data.append('admin_name', this.wndVisitorList.find('input[name="admin_name"]').val());
            this.btn_list.formbutton('start');
            this.list_error.html('');
            $.ajax({
                url: '/ajax/cars/load',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: (r) => {
                    let a = $.parseJSON(r);
                    if (a.status==='ok') {
                        this.find();
                        this.wndVisitorList.iwindow('hide');
                    } else {
                        this.list_error.html(a.message);
                    }
                    this.btn_list.formbutton('stop');
                },
                error: (jqXHR, exception) => {
                    this.list_error.html(jqXHR.responseText);
                    this.btn_list.formbutton('stop');
                }
            });
        }

    }
    get_finder_params() {
        this.find_data.text = this.finder.find('input[name="str_user"]').val();
        this.find_data.status = this.finder.find('select[name="status"]').val();
        this.find_data.date_from = this.finder.find('input[name="date_from"]').val();
        this.find_data.date_to = this.finder.find('input[name="date_to"]').val();
        this.find_data.car_brand = this.finder.find('select[name="car_brand"]').val();
        this.find_data.time_out = this.finder.find('select[name="time_out"]').val();
        this.find_data.car_number = this.finder.find('input[name="car_number"]').val();
        this.find_data.weight = this.finder.find('select[name="weight_id"]').val();
        this.find_data.admin_name = this.finder.find('input[name="admin_name"]').val();
        $('#download_xls').attr('href', '/carxls?find_text='+this.find_data.text+'&find_status='+this.find_data.status+'&find_date_from='+this.find_data.date_from+'&find_date_to='+this.find_data.date_to+'&car_brand='+this.find_data.car_brand+'&car_number='+this.find_data.car_number+'&weight='+this.find_data.weight+'&admin_name='+this.find_data.admin_name+'&time_out='+this.find_data.time_out);
    }

    find() {
        this.get_finder_params();
        this.i = 0;
        this.hasItems = true;
        this.loading = false;
        this.items.html('');
        this.load_items();
    }
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.items = this.el.find('table tbody');
            this.iload = this.el.find('.iload');
            this.finder = this.el.find('.items_finder');
            this.wndVisitorList = $('.window[rel="visitor_list"]');
            this.btn_list = this.wndVisitorList.find('.load_list');
            this.list_error = this.wndVisitorList.find('.error');
            this.find_data = {};

            this.finder.find('input.text').on('keyup', (e)=>{
                if (e.keyCode===13) {
                    this.find();
                }
            });
            this.finder.find('input[name="date_from"], input[name="date_to"]').on('change', (e)=>{
                this.find();
            });
            this.finder.find('select[name="status"]').on('change', (e)=>{
                this.find();
            });
            this.finder.find('select[name="weight_id"]').on('change', (e)=>{
                this.find();
            });
            this.finder.find('select[name="time_out"]').on('change', (e)=>{
                this.find();
            });
            this.finder.find('.find_btn').on('click', ()=>{
                this.find();
            });
            this.items.on('click', 'tr[data-id]', (e)=>{
                location.href='?id='+$(e.currentTarget).data('id');
            });
            let Select2 = new UserSelect2(this.finder.find('select[name="company_user_id"]'), ()=>{
                this.find();
            });
            Select2 = new CarSelect2(this.finder.find('select[name="car_brand"]'), ()=>{
                this.find();
            });

            this.el.find('.load_visitor_list').on('click', ()=>{
                this.wndVisitorList.iwindow('show');
            });
            this.btn_list.on('click', ()=>{
                this.load_list();
            });

            this.find();
            $(window).scroll(() => {
                if ((this.hasItems) && (parseInt($(window).scrollTop()) + parseInt($(window).height()) > parseInt($(document).height()) - 100)) {
                    this.load_items();
                }
            });
        }
    }
}